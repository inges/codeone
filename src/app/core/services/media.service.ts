import { Injectable } from '@angular/core';
import {Platform} from '@ionic/angular';
import {FileChooser} from '@ionic-native/file-chooser/ngx';
import {FilePath} from '@ionic-native/file-path/ngx';
import {IOSFilePicker} from '@ionic-native/file-picker/ngx';
import {CaptureImageOptions, MediaFile, MediaCapture, CaptureError} from '@ionic-native/media-capture/ngx';
import {ImagePicker, ImagePickerOptions} from '@ionic-native/image-picker/ngx';
import {FileTransfer, FileTransferObject, FileUploadOptions} from '@ionic-native/file-transfer/ngx';
import {Base64} from '@ionic-native/base64/ngx';
import {getRandomInt} from '../utils/functions';
import { File } from '@ionic-native/file/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import {defaultBase64Image} from '../utils/constants';
import {uuidv4} from '../utils/functions';

const ENDPOINT = 'https://api.markham.edu.pe/communicator_sdo/test.cfm';

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  constructor(
    private platform: Platform,
    private fileChooser: FileChooser,
    private filePath: FilePath,
    private filePicker: IOSFilePicker,
    private mediaCapture: MediaCapture,
    private imagePicker: ImagePicker,
    private transfer: FileTransfer,
    private base64: Base64,
    private file: File,
    private camera: Camera
  ) { }
  /*
  mediaChooser(): Promise<string> {
    if (!this.isHybrid) { return this.generateFileTest; }
    return new Promise((resolve, reject) => {
      if (this.isAndroid) {
        this.fileChooser.open()
          .then(path => {
            this.filePath.resolveNativePath(path)
              .then(uri => resolve(uri))
              .catch(err => reject(err));
          })
          .catch(error => reject(error));
      } else if (this.isIos)  {
        this.filePicker.pickFile()
          .then(uri =>  resolve(uri))
          .catch(err => reject(err));
      } else { reject('Wrong device'); }
    });
  }
  */

  mediaGallery(): Promise<string> {
    if (!this.isHybrid) { return this.generateFileTest; }
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    };
    return new Promise((resolve, reject) => {
      this.camera.getPicture(options)
        .then((imageData: string) => resolve('data:image/jpeg;base64,' + imageData))
        .catch((err) => reject(err));
    });
  }

  // v2
  mediaCamera(): Promise<string> {
    if (!this.isHybrid) { return this.generateFileTest; }
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA
    };
    return new Promise((resolve, reject) => {
      this.camera.getPicture(options)
        .then((imageData: string) => resolve('data:image/jpeg;base64,' + imageData))
        .catch((err) => reject(err));
    });
  }

  /*
  V1
  mediaGallery(): Promise<string> {
    if (!this.isHybrid) { return this.generateFileTest; }
    const options: ImagePickerOptions = {
      maximumImagesCount: 1,
      quality: 50,
      outputType: 0
    };
    return new Promise((resolve, reject) => {
      this.imagePicker.getPictures(options)
        .then((data: string[]) => {
          let uri = data[0];
          if (this.isIos) { uri = uri.replace('file://',''); }
          resolve(uri);
        })
        .catch(err => reject(err));
    });
  }

  mediaCamera(): Promise<string> {
    if (!this.isHybrid) { return this.generateFileTest; }
    const options: CaptureImageOptions = { limit: 1 };
    return new Promise((resolve, reject) => {
      this.mediaCapture.captureImage(options)
        .then((data: MediaFile[]) => resolve(data[0].fullPath))
        .catch((err: CaptureError) => reject(err));
    });
  }
  */

  /*
  V1: FileUri Upload
  mediaUpload(imageURI: string) {
    const fileTransfer: FileTransferObject = this.transfer.create();
    const path = imageURI.split('/');
    const fileName = path[path.length - 1];
    const options: FileUploadOptions = {
      fileKey: 'file',
      fileName,
      params: {name: fileName},
    };
    return fileTransfer.upload(imageURI, ENDPOINT, options);
  }
  */

  // V2: Base64 Upload
  mediaUpload(imageURI: string): Promise<string> {
    /*
    const fileName = imageURI.split('/').pop();
    const path = imageURI.substring(0, imageURI.lastIndexOf('/') + 1);

    return this.file.readAsDataURL(path, fileName);
    */
    return this.base64.encodeFile(imageURI);
  }

  private get isIos() {
    return this.platform.is('ios');
  }

  private get isAndroid() {
    return this.platform.is('android');
  }

  private get isHybrid() {
    return this.platform.is('hybrid');
  }

  private get webError(): Promise<string> {
    return Promise.resolve('Please run in emulator or real device');
  }

  private get generateFileTest(): Promise<string> {
    return Promise.resolve(defaultBase64Image);
  }

  generateFileName(): string {
    return `${uuidv4()}.jpg`;
  }

  /* V1
  private get generateFileTest(): Promise<string> {
    const id = getRandomInt(1000, 9999);
    return Promise.resolve(`file/file${id}.jpg`);
  }
  */
}
