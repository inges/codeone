import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {forkJoin, of, timer} from 'rxjs';
import {catchError, delay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UbigeoService {

  constructor(private http: HttpClient) { }

  getData() {
    return forkJoin({
      departments: this.http.get('/assets/ubigeo/departments.json'),
      provinces: this.http.get('/assets/ubigeo/provinces.json'),
      districts: this.http.get('/assets/ubigeo/districts.json'),
    });
  }
}
