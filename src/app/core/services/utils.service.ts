import { Injectable } from '@angular/core';
import {AlertController, Platform} from '@ionic/angular';
import {Network} from '@ionic-native/network/ngx';
import {Device} from '@ionic-native/device/ngx';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
    private platform: Platform,
    private alertController: AlertController,
    private network: Network,
    private device: Device,
  ) { }

  isHybrid() {
    return this.platform.is('hybrid');
  }

  checkConnection() {
    if (!this.isHybrid()) { return true; }
    return (this.network.type && (this.network.type !== this.network.Connection.NONE));
  }

  async presentAlert(options?) {
    const alert = await this.alertController.create(options);
    await alert.present();
    return alert;
  }

  async presentBasicAlert(header, message?) {
    return await this.presentAlert({header, message});
  }

  getDeviceUUID(): string {
    if (this.isHybrid()) { return '0'; }
    return this.device.uuid;
  }
}
