export class ISunat {
  ErrMsg: string;
  OK: boolean;
  Activo: boolean;
  CIIU: string;
  CodTipoVia: string;
  CodTipoZona: string;
  CodigoDist: string;
  CodigoDpto: string;
  CodigoProv: string;
  Departamento: string;
  Direccion: string;
  Distrito: string;
  DomicilioLegal: string;
  FechaAlta: string;
  FechaBaja: string;
  Habido: boolean;
  Nombre: string;
  NombreCIIU: string;
  Numeracion: string;
  Provincia: string;
  Referencia: string;
  Ruc: string;
  TipoEmpresa: string;
  TipoPersona: string;
  TipoVia: string;
  TipoZona: string;
  Ubigeo: string;
  Via: string;
  Zona: string;
}

export class ISunatResponse {
  Post_SunatResult: ISunat;
}
