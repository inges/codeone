import { Component, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-consult-result',
  templateUrl: './consult-result.page.html',
  styleUrls: ['./consult-result.page.scss'],
})
export class ConsultResultPage implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }

  next() {
      this.navCtrl.navigateForward('/consult/step3');
  }

  back() {
      this.navCtrl.navigateBack('/consult/step1');
  }
}
