import { Component, OnInit } from '@angular/core';
import {ActionSheetController, AlertController, NavController} from '@ionic/angular';
import {ComplaintType} from '../../core/utils/constants';

@Component({
  selector: 'app-complaint',
  templateUrl: './complaint.page.html',
  styleUrls: ['./complaint.page.scss'],
})
export class ComplaintPage implements OnInit {

  constructor(
      private navCtrl: NavController,
      private alertCtrl: AlertController,
      private actionSheetCtrl: ActionSheetController,
  ) { }

  ngOnInit() {
  }

  back() {
    this.navCtrl.navigateBack('/home');
  }

  async goIdentifiedComplaint() {
    await this.presentActionSheet();
  }

  goAnonymousComplaint() {
    this.navCtrl.navigateForward(['/complaint', ComplaintType.anonymous, 'step0']);
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Persona Natural',
          handler: () => {
            this.navCtrl.navigateForward(['/complaint', ComplaintType.natural, 'step0']);
          }
        },
        {
          text: 'Persona Jurídica',
          handler: () => {
            this.navCtrl.navigateForward(['/complaint', ComplaintType.legal, 'step0']);
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async presentAlertRadio() {
    const alert = await this.alertCtrl.create({
      header: 'Persona',
      inputs: [
        {
          name: 'radio1',
          type: 'radio',
          label: 'Natural',
          value: ComplaintType.natural,
          checked: true
        },
        {
          name: 'radio2',
          type: 'radio',
          label: 'Jurídica',
          value: ComplaintType.legal
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {}
        }, {
          text: 'Continuar',
          handler: (complaintType) => {
            this.navCtrl.navigateForward(['/complaint', complaintType, 'step0']);
          }
        }
      ]
    });
    await alert.present();
  }

}
