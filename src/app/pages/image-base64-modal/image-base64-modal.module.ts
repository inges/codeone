import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImageBase64ModalPage } from './image-base64-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  entryComponents: [ImageBase64ModalPage],
  declarations: [ImageBase64ModalPage]
})
export class ImageBase64ModalPageModule {}
