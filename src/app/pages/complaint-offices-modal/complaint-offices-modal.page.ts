import {Component, Input, OnInit} from '@angular/core';
import {ApiService} from '../../core/services/api.service';
import {IOffice} from '../../core/models/office';
import {cleanTick} from '../../core/utils/functions';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-complaint-offices-modal',
  templateUrl: './complaint-offices-modal.page.html',
  styleUrls: ['./complaint-offices-modal.page.scss'],
})
export class ComplaintOfficesModalPage {
  q: string;
  @Input() office: IOffice;
  @Input() records: IOffice[] = [];
  loading: boolean;

  constructor(
    private api: ApiService,
    private modalCtrl: ModalController,
  ) { }

  ionViewDidEnter() {
    if (!this.records.length) {
      this.load();
    }
  }

  load() {
    this.loading = true;
    this.api.getOffices()
      .subscribe(
        (res: IOffice[]) => {
          this.loading = false;
          this.records = res;
        },
        err => {
          this.loading = false;
          console.log(err);
        }
      );
  }

  get offices(): IOffice[] {
    if (this.q) {
      return this.records.filter(item => cleanTick(item.NOMBRE).includes(cleanTick(this.q)));
    }
    return this.records;
  }

  set offices(records) {
    this.records = records;
  }

  async select(office: IOffice) {
    await this.closeModal(office);
  }

  isSelected(office: IOffice): boolean {
    return (this.office && this.office.codigo === office.codigo);
  }

  async closeModal(office?: IOffice) {
    await this.modalCtrl.dismiss({
      office,
      offices: this.records
    });
  }
}
