import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ComplaintStep2Page } from './complaint-step2.page';
import {ComponentsModule} from '../../shared/components/components.module';
import {ComplaintOfficesModalPageModule} from '../complaint-offices-modal/complaint-offices-modal.module';

const routes: Routes = [
  {
    path: '',
    component: ComplaintStep2Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    ReactiveFormsModule,
    ComplaintOfficesModalPageModule
  ],
  declarations: [ComplaintStep2Page]
})
export class ComplaintStep2PageModule {}
