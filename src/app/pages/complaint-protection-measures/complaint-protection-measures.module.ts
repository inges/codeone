import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ComplaintProtectionMeasuresPage } from './complaint-protection-measures.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    ComplaintProtectionMeasuresPage
  ],
  declarations: [
    ComplaintProtectionMeasuresPage
  ]
})
export class ComplaintProtectionMeasuresPageModule {}
