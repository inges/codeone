import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ComplaintStep4Page } from './complaint-step4.page';
import { ComponentsModule } from '../../shared/components/components.module';
import {ComplaintProtectionMeasuresPageModule} from '../complaint-protection-measures/complaint-protection-measures.module';
import {ImageBase64ModalPageModule} from '../image-base64-modal/image-base64-modal.module';

const routes: Routes = [
  {
    path: '',
    component: ComplaintStep4Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    ComplaintProtectionMeasuresPageModule,
    ImageBase64ModalPageModule
  ],
  declarations: [ComplaintStep4Page]
})
export class ComplaintStep4PageModule {}
