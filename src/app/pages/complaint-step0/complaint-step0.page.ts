import {Component, OnDestroy, OnInit} from '@angular/core';
import {AlertController, LoadingController, NavController, Platform} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {ComplaintType, DocumentType} from '../../core/utils/constants';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {ApiService} from '../../core/services/api.service';
import {Subscription} from 'rxjs';
import {IReniecResponse} from '../../core/models/reniec';
import {ISunatResponse} from '../../core/models/sunat';

export class AppValidators {
  static lengthNumber(length: number): ValidatorFn {
    return (c: AbstractControl): { [key: string]: string } | null => {
      if (c.value && String(c.value).length !== length) {
        return { lengthNumber: `Ingrese ${length} caracteres` };
      }
      return null;
    };
  }
}

@Component({
  selector: 'app-complaint-step0',
  templateUrl: './complaint-step0.page.html',
  styleUrls: ['./complaint-step0.page.scss'],
})
export class ComplaintStep0Page implements OnInit, OnDestroy {
  complaintType;
  complaintTypes = ComplaintType;
  DocumentTypes = DocumentType;

  formAnonymous: FormGroup;
  formLegalNatural: FormGroup;
  fieldDocumentNumber: FormControl;

  backButtonAction: Subscription;

  constructor(
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private api: ApiService,
    private alertCtrl: AlertController,
    private platform: Platform,
    private loadingCtrl: LoadingController,
  ) {
    this.initForms();
  }

  ngOnInit() {
    this.backButtonAction = this.platform.backButton.subscribeWithPriority(9999, () => this.back());
    this.complaintType = this.route.snapshot.paramMap.get('complaintType');
    this.api.initialize(this.complaintType);
    this.initFields();
    this.updateFields();
  }

  ngOnDestroy() {
    this.backButtonAction.unsubscribe();
  }

  get title() {
    switch (this.complaintType) {
      case ComplaintType.anonymous:
        return 'Denuncia Anónima';
      case ComplaintType.natural:
      case ComplaintType.legal:
        return 'Formulario de Denuncias';
    }
  }

  get form(): FormGroup {
    switch (this.complaintType) {
      case ComplaintType.anonymous:
        return this.formAnonymous;
      case ComplaintType.natural:
      case ComplaintType.legal:
        return this.formLegalNatural;
    }
  }

  initForms() {
    this.formAnonymous = this.fb.group({
      phone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
    });
    this.fieldDocumentNumber = new FormControl('');
    this.formLegalNatural = this.fb.group({
      documentType: ['', Validators.required],
      documentNumber: this.fieldDocumentNumber,
      motherLastName: '',
      fatherLastName: '',
      fullName: ['', Validators.required],
      address: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
    });
  }

  initFields() {
    switch (this.complaintType) {
      case ComplaintType.anonymous:
        this.formAnonymous.patchValue({
          phone: this.api.complaint.phone,
          email: this.api.complaint.email
        });
        break;
      case ComplaintType.natural:
        this.fieldDocumentNumber.setValidators([Validators.required, AppValidators.lengthNumber(8)]);
        this.formLegalNatural.controls.motherLastName.setValidators([Validators.required]);
        this.formLegalNatural.controls.fatherLastName.setValidators([Validators.required]);
        this.formLegalNatural.patchValue({
          documentType: this.api.complaint.documentType,
          documentNumber: this.api.complaint.documentNumber,
          motherLastName: this.api.complaint.motherLastName,
          fatherLastName: this.api.complaint.fatherLastName,
          fullName: this.api.complaint.fullName,
          address: this.api.complaint.address,
          phone: this.api.complaint.phone,
          email: this.api.complaint.email
        });
        break;
      case ComplaintType.legal:
        this.fieldDocumentNumber.setValidators([Validators.required, AppValidators.lengthNumber(11)]);
        this.formLegalNatural.patchValue({
          documentType: this.api.complaint.documentType,
          documentNumber: this.api.complaint.documentNumber,
          motherLastName: this.api.complaint.motherLastName,
          fatherLastName: this.api.complaint.fatherLastName,
          fullName: this.api.complaint.fullName,
          address: this.api.complaint.address,
          phone: this.api.complaint.phone,
          email: this.api.complaint.email
        });
        break;
    }
  }

  updateFields() {
    this.formAnonymous.valueChanges.subscribe(values => {
      this.api.complaint.phone = values.phone;
      this.api.complaint.email = values.email;
    });

    this.formLegalNatural.valueChanges.subscribe(values => {
      this.api.complaint.documentType = values.documentType;
      this.api.complaint.documentNumber = values.documentNumber;
      this.api.complaint.fatherLastName = values.fatherLastName;
      this.api.complaint.motherLastName = values.motherLastName;
      this.api.complaint.fullName = values.fullName;
      this.api.complaint.address = values.address;

      this.api.complaint.phone = values.phone;
      this.api.complaint.email = values.email;
    });

  }

  next() {
    this.navCtrl.navigateForward(['/complaint', this.complaintType, 'step1']);
  }

  async back() {
    const alert = await this.alertCtrl.create({
      header: 'Mensaje',
      message: '¿Está seguro de salir del formulario?, no se guardara su registro.',
      buttons: [
        {
          text: 'No',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.navCtrl.navigateBack('/complaint');


          }
        }
      ]
    });
    await alert.present();
  }

  async searchReniec() {
    const loading = await this.loadingCtrl.create({});
    loading.present();
    const documentNumber = this.form.value.documentNumber;
    this.api.getReniecInfo(documentNumber)
      .subscribe(
        (res: IReniecResponse) => {
          loading.dismiss();
          const response = res.Post_ReniecResult;
          if (response.OK) {
            this.formLegalNatural.patchValue({
              motherLastName: response.ApellidoMaterno.trim(),
              fatherLastName: response.ApellidoPaterno.trim(),
              fullName: response.Nombres.trim(),
              address: response.Direccion
            });
          } else {
            this.alertCtrl.create({message: `El DNI Nro. ${documentNumber} no ha sido encontrado`});
          }
          console.log(res);
        },
        err => {
          loading.dismiss();
          console.log(err);
        }
      );
  }

  async searchSunat() {
    const loading = await this.loadingCtrl.create({});
    loading.present();
    const documentNumber = this.form.value.documentNumber;
    this.api.getSunatInfo(documentNumber)
      .subscribe(
        (res: ISunatResponse) => {
          loading.dismiss();
          const response = res.Post_SunatResult;
          if (response.OK) {
            this.formLegalNatural.patchValue({
              fullName: response.Nombre.trim(),
              address: response.Direccion.trim()
            });
          } else {
            this.alertCtrl.create({message: `El RUC Nro. ${documentNumber} no ha sido encontrado`});
          }
          console.log(res);
        },
        err => {
          loading.dismiss();
          console.log(err);
        }
      );
  }
}
